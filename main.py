from math import pi

def get_R_(R = 0):
	while True:
		try:
			R = float(input('please enter R\n'))
		except ValueError:
			print('the R must be a nuuumber please try again')
			continue
		else:
			return R
			break

def calculate_perimeter():
	r = get_R_()
	print(f'the perimeter of this circle is {2 * pi * r}')

calculate_perimeter()